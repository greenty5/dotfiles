# dotfiles

This is a dotfiles maintenance project for my own sake.
When you set up vim environment in new pc, just enter on console like;

sh ./installer.sh ~/dotfiles/vim/dein

- 2022/01/04 adding fonts
    - haranoaji family 

- 2020/11/06 dein.vim installed
    - nerdtree
    - airline-vim
